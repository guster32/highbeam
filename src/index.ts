#!/usr/bin/env node

import { Highlight, AudioProcessor, AudioProcessorSettings } from './AudioProcessor';
import { YouTubeVideoImport, VideoSplicer } from './VideoImport';
import yargs from 'yargs';


var argv = yargs
	.usage('Usage: node highbeam -v [url]')
	.alias('v', 'video')
    .demandOption(['v'])
    .argv;

const audioProcessorSettings: AudioProcessorSettings = {
	refLevel: 1e-8,
	dbMin: 0,
	dbRange: 100,
	freqMinCents: 1200,
	freqRangeCents: 8700,
	blocksPerFFT: 8,
	blockSize: 1024
}
let downloadedVideo:string;


YouTubeVideoImport.fetchAsset(new URL(argv.video))
	.then((dowloadFileName)=> {
		downloadedVideo = dowloadFileName;
		return YouTubeVideoImport.extractAudio(downloadedVideo);
	})
	.then((audioFileName) => {
		return AudioProcessor.findHilights(audioFileName, audioProcessorSettings);
	})
	.then((highlights:Array<Highlight>) => {
		//let parallelSplice:Array<Highlight> = highlights.slice(0, 15); //Max 15 higlights	
		return VideoSplicer.spliceHilights(downloadedVideo, highlights, './output')
	})
	.then(()=>{
		console.log("Hilight extraction completed!");
	})
