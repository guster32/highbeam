export class CircularWordBuffer {
    private head: number;
	private wordSize: number;
	private buffer: Buffer;
	private bpushed: number;
	private bufferSizeInWords: number;

    constructor(bufferSizeInWords:number, wordSize:number) {
		this.wordSize = wordSize;
	    this.head = 0;
		this.bpushed = 0;
		this.bufferSizeInWords = bufferSizeInWords;
	    this.buffer = Buffer.alloc(bufferSizeInWords * this.wordSize);
    }
    public push(blkBuffer:Buffer) :void {
        if (blkBuffer.length % this.wordSize !== 0 || blkBuffer.length > this.buffer.length) {
			throw `The length of buffer push (${blkBuffer.length})
			must be a multiple of the word size (${this.wordSize}) 
			and smaller than the buffer size (${this.buffer.length})`;
		}
		blkBuffer.copy(this.buffer, this.head);
		this.bpushed += blkBuffer.length;
		this.head = this.bpushed === blkBuffer.length ? this.bpushed : this.bpushed % this.buffer.length;
	}
	getSample(wordOffset:number):Buffer {
		let bufferWordOffset = wordOffset * this.wordSize;
		let start = (this.head + bufferWordOffset) % this.buffer.length;
		return this.buffer.slice(start, start + this.wordSize);
	}
	get size():number {
		return this.bpushed >= this.buffer.length ?  this.bufferSizeInWords : this.head / this.wordSize
	}
	get bytesPushed():number {
		return this.bpushed;
	}
}