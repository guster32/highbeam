// Gilián Zoltán <gilian@caesar.elte.hu>, 2014
// Simplified BSD License

// Járai Antal: Bevezetés a matematikába című jegyzet alapján készült.
// Harmadik kiadás, ELTE Eötvös Kiadó, 2009, 332-334 p.,
// 9.2.44. FFT algoritmus.

// based on the discrete mathematics textbook 'Bevezetés a matematikába' by
// Antal Járai (hungarian).

var sin = Math.sin,
    cos = Math.cos,
    pi2 = Math.PI * 2,
    log2 = Math.log(2);

function ReverseBits(x:any, nbits:any) {
	var y = 0;
	for (var j = 0; j < nbits; ++j) {
		y <<= 1;
		y |= (x & 1);
		x >>= 1;
	}
	return y;
}

export class FFTAlgorithm {
	private numBits: number;
	private numBins: number;
	private bitRev: Int32Array;
	private rou: Float32Array;

	constructor(numBins?:any) {
		this.numBins = 1;
		this.numBits = 1;
		this.rou = new Float32Array()
		this.bitRev = new Int32Array;
		this.reset(numBins);
	}
	reset(numBins?:number) {
		this.numBits = Math.ceil(Math.log(numBins || 1) / log2),
		this.numBins = 1 << this.numBits;
		this.createBitRevLUT();
		this.createRootOfUnityLUT();
	}
	createBitRevLUT() {
		this.bitRev = new Int32Array(this.numBins);
		for (var i = 0; i < this.numBins; ++i) {
			this.bitRev[i] = ReverseBits(i, this.numBits);
		}
	}
	createRootOfUnityLUT() {
		var n = this.numBins;
		this.rou = new Float32Array(n);
		for (var i = 0; i < (n >> 1); ++i) {
			var j = ReverseBits(i, this.numBits - 1);
			this.rou[j << 1] = cos(pi2 * i / n);
			this.rou[j << 1 | 1] = -sin(pi2 * i / n);
		}
	}
	forward(array:Float32Array) {
		let n = this.numBins;

		if (array.length != 2 * n) {
			throw 'FFTAlgorithm.Forward: array size should be ' + (2 * n).toString();
		}
	
		for (var l = this.numBins >> 1; l > 0; l >>= 1) {
			for (var k = 0, t = 0; k < n; k += l + l, ++t) {
				var wr = this.rou[t << 1];
				var wi = this.rou[t << 1 | 1];
				for (var j = k; j < k + l; ++j) {
					var xr = array[j << 1];
					var xi = array[j << 1 | 1];
					var zr = array[(j + l) << 1];
					var zi = array[(j + l) << 1 | 1];
					var yr = wr * zr - wi * zi;
					var yi = wr * zi + wi * zr;
					array[j << 1] = xr + yr;
					array[j << 1 | 1] = xi + yi;
					array[(j + l) << 1] = xr - yr;
					array[(j + l) << 1 | 1] = xi - yi;
				}
			}
		}
	
		for (let i = 0; i < n; ++i) {
			let j = this.bitRev[i];
			if (i < j) {
				var tr = array[i << 1];
				var ti = array[i << 1 | 1];
				array[i << 1] = array[j << 1];
				array[i << 1 | 1] = array[j << 1 | 1];
				array[j << 1] = tr;
				array[j << 1 | 1] = ti;
			}
		}
	}
}
