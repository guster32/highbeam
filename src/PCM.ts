/*
 *   PCM file parser. see for format details:
 *   https://www.codeguru.com/cpp/g-m/multimedia/audio/article.php/c8935/PCM-Audio-and-Wave-Files.htm#page-2
 *   
*/
import { StringDecoder, NodeStringDecoder } from 'string_decoder';
import { openSync, readSync, createReadStream } from 'fs';
import { Stream } from 'stream';
const decoder:NodeStringDecoder = new StringDecoder('ascii');

export type PCM_FMT = {
    offset: number,
    szFmtID: string,
    dwFmtSize: number,
    wFormatTag: number,
    wChannels: number,
    dwSamplesPerSec: number,
    dwAvgBytesPerSec: number,
    wBlockAlign: number,
    wBitsPerSample: number
};

export type PCM_RIFF = {
    szRiffID: string,
    dwRiffSize: number,
    szRiffFormat: string
}

export type PCM_DATA = {
    offset: number,
    szDataID: string,
    dwDataSize: number
};

export class PCM {
    private filepath:string;
    private buffer: Buffer;
    private hasMetadata: boolean;
    private fmtBlock: PCM_FMT | undefined;
    private riffBlock: PCM_RIFF | undefined;
    private dataBlock: PCM_DATA | undefined;
    private readonly SIZE: number = 10000; // 10Kb buffer is enough to read wave file format and properties

    constructor(path:string) {
        this.filepath = path;
        this.buffer = Buffer.alloc(this.SIZE);
        this.hasMetadata = false;
    }

    private readbuffer():void {
        let fd = openSync(this.filepath, 'r');
        readSync(fd, this.buffer, 0, this.SIZE, 0);
        this.hasMetadata = true;
    }

    private readRiffBlock(): PCM_RIFF {
        if(!this.hasMetadata) {
            this.readbuffer();
        }
        let currentIndex = 0;
        //Riff block
        return {
            szRiffID: decoder.end(this.buffer.slice(currentIndex, currentIndex+=4)),
            dwRiffSize: this.buffer.slice(currentIndex, currentIndex+=4).readUInt32LE(0),
            szRiffFormat: decoder.end(this.buffer.slice(currentIndex, currentIndex+=4))
        }
    }

    private readFormatBlock(): PCM_FMT {
        if(!this.hasMetadata) {
            this.readbuffer();
        }
        let currentIndex = 0;

        let offset = currentIndex = this.buffer.indexOf("fmt ", currentIndex);
        
        if(currentIndex === -1) {
            throw new Error("Buffer does not contain PCM format information.");
        }
        if(currentIndex + 24 > this.buffer.length) {
            throw new Error("Buffer has only partial PCM format information.");
        }

        //FMT block
        return {
            offset: offset,
            szFmtID: decoder.end(this.buffer.slice(currentIndex, currentIndex+=4)),
            dwFmtSize: this.buffer.slice(currentIndex, currentIndex+=4).readUInt32LE(0),
            wFormatTag: this.buffer.slice(currentIndex,currentIndex+=2).readUInt16LE(0),
            wChannels: this.buffer.slice(currentIndex,currentIndex+=2).readUInt16LE(0),
            dwSamplesPerSec: this.buffer.slice(currentIndex, currentIndex+=4).readUInt32LE(0),
            dwAvgBytesPerSec: this.buffer.slice(currentIndex, currentIndex+=4).readUInt32LE(0),
            wBlockAlign: this.buffer.slice(currentIndex,currentIndex+=2).readUInt16LE(0),
            wBitsPerSample: this.buffer.slice(currentIndex,currentIndex+=2).readUInt16LE(0)
        }
    }

    private readDataBlock(): PCM_DATA {
        if(!this.hasMetadata) {
            this.readbuffer();
        }
        let currentIndex = 0;
        let offset = currentIndex = this.buffer.indexOf("data", currentIndex);

        //Data block
        return {
            offset: offset,
            szDataID: decoder.end(this.buffer.slice(currentIndex, currentIndex+=4)),
            dwDataSize: this.buffer.slice(currentIndex, currentIndex+=4).readInt32LE(0)
        }
    }

    get filePath():string {
        return this.filepath;
    }

    get format():PCM_FMT {
        if(!this.fmtBlock) {
            this.fmtBlock  = this.readFormatBlock();
        }
        return this.fmtBlock;
    }

    get data():PCM_DATA {
        if(!this.dataBlock) {
            this.dataBlock = this.readDataBlock();
        }
        return this.dataBlock;
    }

    get riff():PCM_RIFF {
        if(!this.riffBlock) {
            this.riffBlock = this.readRiffBlock();
        }
        return this.riffBlock;
    }

    public getAudioStream(chunkSize:number): Stream {
        return createReadStream(this.filepath, { highWaterMark: chunkSize, start:this.data.offset + 8});
    }
}