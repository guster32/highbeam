import { createWriteStream, existsSync } from 'fs';
import * as path from 'path';
import { Highlight } from './AudioProcessor';

const ytdl = require('ytdl-core');
const FFmpeg = require('fluent-ffmpeg');

export class VideoSplicer {
    public static async spliceHilights(source:string, hilights:Array<Highlight>, folder?:string) {
        for(let idx = 0; idx < hilights.length; idx++ ) {
            await VideoSplicer.spliceHilight(source, hilights[idx], folder);
        }
    }

    private static async spliceHilight(source:string, hilight:Highlight, folder?:string) {
        let outputFolder = folder || "";
        let spliceName = `${outputFolder}/${hilight.time.replace(/:/g,'_')}.mp4`;
        let spliceStartTime = hilight.time;
        let spliceDuration = hilight.duration.toString();
        return new Promise<string>((resolve, reject) => {
            console.log(`Splicing:${spliceName} with StartTime:${spliceStartTime} Duration:${spliceDuration}`);
            new FFmpeg({ source: source, timeout:0})
                .setStartTime(spliceStartTime)
                .setDuration(spliceDuration)
                .output(spliceName)
                .on('end', function(err:Error) {   
                    if(!err) {
                        console.log(`Completed:${spliceName}`);
                        return resolve(spliceName);
                    }
                    console.log(`Error splicing: ${JSON.stringify(hilight)}. Error: ${err.message}`);
                    reject(err);
                })
                .on('error', function(err:Error) {
                    console.log(`Error splicing: ${JSON.stringify(hilight)}. Error: ${err.message}`);
                    reject(err);
                }).run();
        });
    }
}

export class YouTubeVideoImport {

    public static async fetchAsset(url:URL): Promise<string> {
        return new Promise<string>((resolve, reject) =>{
            let fileName = `${url.searchParams.get("v")}.mp4`;
            if(existsSync(fileName)) {
                return resolve(fileName);
            }
            
            const video = ytdl(url.href, { filter: (format:any) => format.container === 'mp4' });
            video.pipe(createWriteStream(fileName));

            video.once('response', (response:any) => {
                //TODO: We will need to do some error handling and logging here.
                console.log(response);
            });
            video.on('progress', (chunkLength:number, downloaded:number, total:number) => {
                const floatDownloaded = downloaded / total;
                console.log(`${(floatDownloaded * 100).toFixed(2)}% downloaded`);
            });
            video.on('end', () => {
                resolve(fileName);
            });
        });
    }

    public static async extractAudio(filePath:string): Promise<string> {
        return new Promise<string>((resolve, reject) =>{
            let outputFileName = filePath.replace(/\.[^/.]+$/, ".wav");
            if(existsSync(outputFileName)) {
                return resolve(outputFileName);
            }
            
            if(!existsSync(filePath) || path.extname(filePath) !== '.mp4') {
                return reject(`Could not extract audio from file ${filePath}. Make sure the file exists and it is of extension '.mp4'.`);
            }

            new FFmpeg({ source: filePath, timeout:0})
                .withAudioCodec('pcm_f32le')
                .withAudioChannels(1)
                .withAudioFrequency(44100)
                .toFormat('wav')
                .on('start', function() {
                    console.log("Audio Extraction Started");
                })
                .on('error', function(err:Error){
                    console.log(`Audio Extraction Failed. ${err.message}`);
                    reject(err);
                })
                .on('progress', function(progress:any){
                    console.log("Audio Extraction Percentage ..." + progress.percent);
                })
                .on('end', function(){
                    console.log("Audio Extraction Ended");
                    resolve(outputFileName)
                })
                .saveToFile(outputFileName);

        });
    }
}




