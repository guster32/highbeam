import { Transform } from 'stream';
import { PCM_FMT, PCM } from './PCM';
import { CircularWordBuffer } from './CircularWordBuffer';
import { FFTAlgorithm } from './FFTAlgorithm';
import { std, median } from 'mathjs';
import { existsSync } from 'fs';
import { MultiRange } from 'multi-integer-range';
const moment = require('moment');

export type AudioProcessorSettings = {
    refLevel: number,
    dbMin: number,
    dbRange: number,
    freqMinCents: number,
    freqRangeCents: number,
    blockSize: number,
    blocksPerFFT: number
};

export type freqDbTimedInfo = {
    time:number,
    freq:number,
    db: number,
    sd: number,
    stdSd: number,
    stdDb: number
}

export type Highlight = {
    time:string,
    duration:number
}

function trapezoidalSum(x:number[], fx:number[]): number {
    let dx = (x[x.length-1] - x[0]) / (x.length -1);

    let fxSum = fx.reduce((acc, current, currentIndx, arr)=>{
        if(currentIndx === 0 || currentIndx === arr.length-1) {
            return acc + Math.abs(current);
        }
        return acc + (2*Math.abs(current));
    });

    return (dx/2) * fxSum;
};

export class AudioProcessor extends Transform {
    private format: PCM_FMT;
    private options: AudioProcessorSettings;
    private samplesBuffer: CircularWordBuffer;
    private sampleCount: number;
    private fft: FFTAlgorithm;
    private log10 = Math.log(10);
    private timedData: Array<freqDbTimedInfo> = [];
    private sdDeviation: Array<number> = [];
    private dbDevation: Array<number> = [];
    
    constructor(format:PCM_FMT, settings: AudioProcessorSettings) {
        super({});
        this.options = settings;
        this.sampleCount = 0;
        this.format = format;
        this.samplesBuffer = new CircularWordBuffer(this.options.blockSize * this.options.blocksPerFFT, this.format.wBlockAlign);
        this.fft = new FFTAlgorithm(this.options.blockSize * this.options.blocksPerFFT);
    }

    process() {
        let num_samples = this.samplesBuffer.size;
        let samples = new Float32Array(2 * num_samples);
        for (let i = 0; i < num_samples; ++i) {
            samples[2*i+0] = this.samplesBuffer.getSample(i).readFloatLE(0); // real
            samples[2*i+1] = 0; // imag
        }
    
        // apply window
        let fft_size = this.options.blockSize * this.options.blocksPerFFT;
        for (let i = 0; i < num_samples; ++i) {
            let x = 2 * Math.PI * i / (fft_size - 1);
            // Hamming window
            let w = 0.54 - 0.46 * Math.cos(x);
    
            // Nuttall window
            //w = 0.355768 - 0.487396 * Math.cos(x) + 0.144232 * Math.cos(2*x) + 0.012604 * Math.cos(3*x);
            
            samples[2*i+0] *= w;  // real
            samples[2*i+1] *= w;  // imag
        }
    
        // FFT
        this.fft.forward(samples);
    
        // render STFT curve
    
        let freq_res = this.format.dwSamplesPerSec / num_samples;
        let freq_nyquist = this.format.dwSamplesPerSec / 2;
    
        let c0_freq = 16.35;
        let freq_cent = Math.pow(2, 1/1200);
        let freq_min = c0_freq * Math.pow(freq_cent, this.options.freqMinCents);
        let freq_max = c0_freq * Math.pow(freq_cent, this.options.freqMinCents + this.options.freqRangeCents);
    
    
        let freq_step = Math.pow(freq_cent, 10);

        let frequencies:Array<number> = [];
        let dbs:Array<number> = [];
        for (let freq = freq_min, i = 0; freq < Math.min(freq_max, freq_nyquist); freq *= freq_step, ++i) {
            let bin = Math.floor(freq / freq_res)
            let re = samples[bin << 1];
            let im = samples[bin << 1 | 1];
            let fftMagSq = Math.pow(re / num_samples, 2) + Math.pow(im / num_samples, 2);
            //let x = (i * 10 / this.options.freqRangeCents);
            let db = 20 * Math.log(fftMagSq / this.options.refLevel) / this.log10;
            if(db > this.options.dbMin && db < this.options.dbMin + this.options.dbRange) {
                frequencies.push(freq);
                dbs.push(db);
            }
        }
        
        if(frequencies.length > 1 && dbs.length > 1) {
            let mFreq = median(frequencies);
            let spectralDensity = trapezoidalSum(frequencies, dbs); // <== https://en.wikipedia.org/wiki/Spectral_density
            let mDb = median(dbs);
            let curentTime = this.sampleCount / this.format.dwSamplesPerSec;
            let currentDatapoint: freqDbTimedInfo;
            if(mFreq && mDb) {
                currentDatapoint = {
                    time: Math.floor(curentTime),
                    freq: Math.floor(mFreq),
                    db:Math.floor(mDb),
                    sd: spectralDensity,
                    stdSd: 0,
                    stdDb: 0,
                };
                if(this.sdDeviation.length >= 10) {
                    this.sdDeviation.shift();
                    this.sdDeviation.push(spectralDensity);
                    currentDatapoint.stdSd = std(this.sdDeviation);
                } else { this.sdDeviation.push(spectralDensity) }
                if(this.dbDevation.length >= 10) {
                    this.dbDevation.shift();
                    this.dbDevation.push(mDb);
                    currentDatapoint.stdDb = std(this.dbDevation);
                }  else { this.dbDevation.push(mDb) }
                this.timedData.push(currentDatapoint);
            }
        }
        //console.log(`curentTime:${curentTime} Freq: ${mFreq} DBRange:${mDb}`);
    }
    _transform(chunk:Buffer, encoding:any, cb: Function) {
        this.samplesBuffer.push(chunk);
        this.sampleCount += chunk.length / this.format.wBlockAlign;
        let overflow = (this.samplesBuffer.bytesPushed % (this.options.blockSize * this.options.blocksPerFFT* this.format.wBlockAlign)) === 0;
        if(this.samplesBuffer.bytesPushed > 0 && overflow) {
            this.process();
        }

        this.push(chunk);
        cb();
    }
    private writeDataToFile(fileName:string):void {
        var fs = require('fs')
        var logger = fs.createWriteStream(fileName, { flags: 'w' });
        this.timedData.forEach((data) =>{
            logger.write(`${data.freq},${data.db},${data.time},${data.sd},${data.stdDb},${data.stdSd}\n`);
        })

    }
    private get stdDb(): number {
        let targetDb = std(this.timedData.map(td=>td.db));
        console.log(`STDEV DBs: ${targetDb}`);
        return targetDb;
    }
    private get stdSd(): number {
        let targetSd = std(this.timedData.map(td=>td.sd));
        console.log(`STDEV SpectralDesity: ${targetSd}`);
        return targetSd;
    }
    private get highLight(): Array<Highlight> {
        let targetDb = this.stdDb;
        let targetsd = this.stdSd;
        let data = this.timedData
            .filter(td => {
                return td.stdSd && td.stdDb && (td.stdSd > targetsd && td.stdDb > targetDb);
            });
        let theGoodStuff = new MultiRange(data.map(td=>td.time)).getRanges()
            .map((range:Array<number>)=>{
                let adjustedStart = (range[0] - 5) < 0 ? 0 : (range[0] - 5);
                let adjustedEnd = range[1] + 5;
                return {
                    time: moment().startOf('day').seconds(adjustedStart).format('H:mm:ss'),
                    duration: adjustedEnd - adjustedStart
                }
            })
            .filter(timeRange => {
                return timeRange.duration > 12; //Only get stuff with more than 2 seconds of intensity audio.
            });
        return theGoodStuff;
    }

    public static async findHilights(filePath:string, settings:AudioProcessorSettings): Promise<Array<Highlight>> {
        return new Promise<Array<Highlight>>((resolve, reject) => {
            if(!existsSync(filePath)) {
                return reject(`Could not extract highlight from audio file ${filePath}. Make sure the file exists.`);
            }
            const pcm = new PCM(filePath);
            const readChunckSize = settings.blockSize * pcm.format.wBlockAlign;
            const audioProcessor = new AudioProcessor(pcm.format, settings);

            console.log(`Started processing audio file.`);
            pcm.getAudioStream(readChunckSize)
                .pipe(audioProcessor)
                .on("data", (data:Buffer) => {
                    //TODO:Add progress here
                })
                .on("error", (err:Error) =>{
                    console.log(`Failed to process audio file. Error: ${err.message}`);
                    reject(err)
                })
                .on("end", () => {
                    audioProcessor.writeDataToFile(`data.txt`);
                    console.log(`Completed processing audio file.`);
                    resolve(audioProcessor.highLight);
                });
        });
    }
}