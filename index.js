"use strict";
//Extract Audio as wav from video.
//Create sound array form wav file.
//calculate avg volume and stdev in 1 sec chunks
//Split audio into small chucks of 10 sec
//Post each chunck in parallel to google apis
//
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// const fs = require('fs');
// const ytdl = require('ytdl-core');
// const FFmpeg = require('fluent-ffmpeg');
// import { ffprobe } from 'fluent-ffmpeg';
const fs_1 = require("fs");
const audio_analyser_1 = __importDefault(require("audio-analyser"));
var analyser = audio_analyser_1.default({
    fftSize: 64
});
analyser.on('data', function (chunk) {
    //console.log(chunk);
    // var floatFreq = analyser.getFloatFrequencyData(new Float32Array(analyser.fftSize));
    // var floatTime = this.getFloatTimeDomainData(new Float32Array(this.fftSize));
    // var byteFreq = this.getByteFrequencyData(new Uint8Array(this.fftSize));
    // var byteTime = this.getByteTimeDomainData(new Uint8Array(this.fftSize));
    // var freq = this.getFrequencyData();
    // var time = analyser.getTimeData();
    // console.log(time);
    // assert(almost(floatFreq[0], freq[0], err, err));
    // // assert(almost(byteFreq[0], freq[0], err, err));
    // assert.equal(floatFreq.length, freq.length);
    // assert.equal(byteFreq.length, freq.length);
    // assert(almost(floatTime[0], time[0], err, err));
    // assert(almost(pcm.convertSample(byteTime[0], {signed: false, bitDepth: 8}, {float: true}), time[0], err, err));
    // assert.equal(floatTime.length, time.length);
    // assert.equal(byteTime.length, time.length);
});
fs_1.createReadStream('./assets/test.wav')
    .pipe(analyser)
    .pipe(fs_1.createWriteStream('./assets/test2.wav'));
// ytdl('https://www.youtube.com/watch?v=0yq3gsTr7H4', { filter: (format) => format.container === 'mp4' })
//  .pipe(fs.createWriteStream('0yq3gsTr7H4.mp4'));
//const command = new FfmpegCommand("../test/0yq3gsTr7H4.mp4");
// ffprobe('./src/test/0yq3gsTr7H4.mp4', (err, data) => {
//     console.log(data.format.duration);
// });
// new FFmpeg({ source: './src/test/asset/0yq3gsTr7H4.mp4', timeout:0})
//     .withAudioCodec('libmp3lame')                            
//     .withAudioBitrate(128)
//     .withAudioChannels(1)
//     .withAudioFrequency(44100)
//     .toFormat('wav')
//     .on('start', function() {
//         console.log(" DEBUG: " + "Audio Extraction Started");
//     })
//     .on('error', function(err:any){
//         console.log(" DEBUG: " + "Audio Extraction Failed."); console.log(err);
//     })
//     .on('progress', function(progress:any){
//         console.log(" DEBUG: " + "Audio Extraction Percentage ..." + progress.percent);
//     })
//     .on('end', function(){
//         console.log(" DEBUG: " + "Audio Extraction Ended");
//     })
//     .saveToFile("test.wav"); 
// let readStream = fs.createReadStream("L71nt62ddPk.mp4");
// // Imports the Google Cloud client library
// const speech = require('@google-cloud/speech');
// const fs = require('fs');
// // Creates a client
// const client = new speech.SpeechClient();
// // The name of the audio file to transcribe
// const fileName = './first10Mono.wav';
// // Reads a local audio file and converts it to base64
// const file = fs.readFileSync(fileName);
// const audioBytes = file.toString('base64');
// // The audio file's encoding, sample rate in hertz, and BCP-47 language code
// const audio = {
//     content: audioBytes,
// };
// const config = {
//     encoding: 'LINEAR16',
//     languageCode: 'en-US',
// };
// const request = {
//     audio: audio,
//     config: config,
// };
// // Detects speech in the audio file
// client
//     .recognize(request)
//     .then(data => {
//         const response = data[0];
//         const transcription = response.results
//             .map(result => result.alternatives[0].transcript)
//             .join('\n');
//         console.log(`Transcription: ${transcription}`);
//     })
//     .catch(err => {
//         console.error('ERROR:', err);
//     });
//# sourceMappingURL=index.js.map